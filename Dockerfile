# Use the LTS release.
FROM python:2.7
COPY . /app
WORKDIR /app
RUN yum install -y python-pip
RUN pip install -r requirements.txt
ENTRYPOINT ["python"]
CMD ["app.py"]
